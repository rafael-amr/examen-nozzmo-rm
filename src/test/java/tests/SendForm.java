package tests;

import commons.BaseTest;
import org.testng.annotations.Test;
import pages.FrontPage;

public class SendForm extends BaseTest {

    @Test(priority = 1, description = "Try to send the form with only the name on it.")
    public void sendOnlyName() {
        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.enterName("Name");
        frontPage.clickSendButton();
        softAssert.assertTrue(frontPage.checkEmailIsRequiredWarning());
        frontPage.clearName();
    }

    @Test(priority = 2, description = "Try to send the form with only the email on it.")
    public void sendOnlyEmail() {
        FrontPage frontPage = new FrontPage(driver);

        frontPage.enterEmail("email@email.com");
        frontPage.clickSendButton();
        softAssert.assertTrue(frontPage.checkNameIsRequiredWarning());
        frontPage.clearEmail();
    }

    @Test(priority = 3, description = "Try to send the form with only the description on it.")
    public void sendOnlyDescription() {
        FrontPage frontPage = new FrontPage(driver);

        frontPage.enterDescription("Description");
        frontPage.clickSendButton();
        softAssert.assertTrue(frontPage.checkNameIsRequiredWarning());
        frontPage.clearDescription();
    }

    @Test(priority = 4, description = "Try to send the form with an invalid email on it.")
    public void sendInvalidEmail() {
        FrontPage frontPage = new FrontPage(driver);

        frontPage.enterName("Name");
        frontPage.enterEmail("invalidEmail");
        frontPage.enterDescription("Description");
        frontPage.clickSendButton();
        softAssert.assertTrue(frontPage.checkInvalidEmailWarning());
        frontPage.clearName();
        frontPage.clearEmail();
        frontPage.clearDescription();
    }

    @Test(priority = 5, description = "Try to successfully send the form.")
    public void sendFormSuccessfully() {

        FrontPage frontPage = new FrontPage(this.driver);

        frontPage.enterName("Name");
        frontPage.enterEmail("email@email.com");
        frontPage.enterDescription("Description");
        frontPage.clickSendButton();
        softAssert.assertTrue(frontPage.checkReceivedConfirmation());

        softAssert.assertAll();
    }
}
