package tests;

import commons.BaseTest;
import org.testng.annotations.Test;
import pages.FrontPage;

public class CheckProcessDetails extends BaseTest {

    @Test(description = "Check that all the details of the process display correctly.")
    public void checkProcessDetails() {

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickUnderstandingYourNeedsHeader();
        frontPage.clickSourcingCandidatesHeader();
        frontPage.clickTechnicalEvaluationHeader();
        frontPage.clickCulturalFitEvaluationHeader();
        frontPage.clickDataDrivenInsightsHeader();
        frontPage.clickfinalSelectionHeader();

        softAssert.assertTrue(frontPage.checkUnderstandingYourNeedsDetail());
        softAssert.assertTrue(frontPage.checkSourcingCandidatesDetail());
        softAssert.assertTrue(frontPage.checkTechnicalEvaluationDetail());
        softAssert.assertTrue(frontPage.checkCulturalFitDetail());
        softAssert.assertTrue(frontPage.checkDataDrivenInsightsDetail());
        softAssert.assertTrue(frontPage.checkFinalSelectionDetail());
    }
}
