package tests;

import commons.BaseTest;
import org.testng.annotations.Test;
import pages.FrontPage;

public class CheckPageSections extends BaseTest {

    @Test(description = "Check that all the sections of the front page are displayed.")
    public void checkFrontPageSections() {

        FrontPage frontPage = new FrontPage(this.driver);

        frontPage.open();

        softAssert.assertTrue(frontPage.checkYourPartnerForHiringText(), "The 'Your Partner For Hiring' header was not displayed.");
        softAssert.assertTrue(frontPage.checkWeAssessCandidatesText(), "The 'We Assess Candidates' header was not displayed.");
        softAssert.assertTrue(frontPage.checkhowWeHelpText(), "The 'How Can We Help?' header was not displayed.");
        softAssert.assertTrue(frontPage.checkdiscoverHowWeHelpedText(), "The 'Discover How We Helped' header was not displayed.");
        softAssert.assertTrue(frontPage.checkourProcessText(), "The 'Check Our Process' header was not displayed.");
        softAssert.assertTrue(frontPage.checkOurTeamText(), "The 'Check Our Team' header was not displayed.");

        softAssert.assertAll();
    }
}
