package tests;

import commons.BaseTest;
import org.testng.annotations.Test;
import pages.FrontPage;

public class CheckLinks extends BaseTest {

    @Test(priority = 1, description = "Test the link on the header of the page.")
    public void checkHeaderLink() {
        final String EXPECTED_URL = "https://google.com";

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickHeaderLink();
        softAssert.assertEquals(driver.getCurrentUrl(), EXPECTED_URL);

        driver.navigate().back();
    }

    @Test(priority = 2, description = "Test the link on the 'Candidates' section of the page.")
    public void checkCandidatesLink() {
        final String EXPECTED_URL = "https://google.com";

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickCandidatesLink();
        softAssert.assertEquals(driver.getCurrentUrl(), EXPECTED_URL);

        driver.navigate().back();
    }

    @Test(priority = 3, description = "Test the link on the 'Increase' section of the page.")
    public void checkIncreaseLink() {
        final String EXPECTED_URL = "https://google.com";

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickIncreaseLink();
        softAssert.assertEquals(driver.getCurrentUrl(), EXPECTED_URL);

        driver.navigate().back();
    }

    @Test(priority = 4, description = "Test the link on the 'Free' section of the page.")
    public void checkFreeLink() {
        final String EXPECTED_URL = "https://google.com";

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickFreeLink();
        softAssert.assertEquals(driver.getCurrentUrl(), EXPECTED_URL);

        driver.navigate().back();
    }

    @Test(priority = 5, description = "Test the link on the 'Next Level' section of the page.")
    public void checkNextLevelLink() {
        final String EXPECTED_URL = "https://google.com";

        FrontPage frontPage = new FrontPage(driver);
        frontPage.open();

        frontPage.clickNextLevelLink();
        softAssert.assertEquals(driver.getCurrentUrl(), EXPECTED_URL);

        driver.navigate().back();
    }
}
