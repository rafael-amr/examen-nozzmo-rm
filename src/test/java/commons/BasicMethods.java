package commons;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasicMethods {

    JavascriptExecutor js;

    public static void scrollToElement(WebElement element, WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static boolean checkElement(WebElement element, WebDriver driver, WebDriverWait wait) {
        try {
            scrollToElement(element, driver);
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (Exception e) {
            scrollToElement(element, driver);
            return element.isDisplayed();
        }
    }

    public static void click(WebElement element, WebDriver driver, WebDriverWait wait) {
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
        } catch (Exception e) {
            System.out.println("Element not found within the timeout period. Attempting to scroll");
            scrollToElement(element, driver);
            element.click();
            e.printStackTrace();
        }
    }

    public static void enterText(WebElement element, WebDriver driver, WebDriverWait wait, String text) {
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            element.clear();
            element.sendKeys(text);
        } catch (Exception e) {
            System.out.println("Element not found within the timeout period. Attempting to scroll.");
            scrollToElement(element, driver);
            element.clear();
            element.sendKeys(text);
            e.printStackTrace();
        }
    }

    public static void clearText(WebElement element, WebDriver driver, WebDriverWait wait) {
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            element.clear();
        } catch (Exception e) {
            System.out.println("Element not found within the timeout period. Attempting to scroll.");
            scrollToElement(element, driver);
            element.clear();
            e.printStackTrace();
        }
    }
}
