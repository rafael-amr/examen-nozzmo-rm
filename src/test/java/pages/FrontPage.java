package pages;

import commons.BasicMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FrontPage {

    private WebDriver driver;
    protected WebDriverWait shortWait;
    protected WebDriverWait mediumWait;
    protected WebDriverWait longWait;

    // Page URL
    private static final String PAGE_URL = "https://nozzmo.com/recruiting";

    // WebElements
    @FindBy(xpath = "//span[contains(text(), 'Your Partner for Hiring')]")
    private WebElement yourPartnerForHiringText;

    @FindBy(xpath = "//span[contains(text(), 'We assess candidates')]")
    private WebElement weAssessCandidatesText;

    @FindBy(xpath = "//span[contains(text(), 'How can we help you?')]")
    private WebElement howWeCanHelpText;

    @FindBy(xpath = "//span[contains(text(), 'Discover how we helped')]")
    private WebElement discoverHowWeHelpedText;

    @FindBy(xpath = "//span[contains(text(), 'Our')]//strong[contains(text(), 'Process')]")
    private WebElement ourProcessText;

    @FindBy(xpath = "//span[contains(text(), 'Our')]//strong[contains(text(), 'team')]")
    private WebElement ourTeamText;

    @FindBy(css = "input#name")
    private WebElement nameInput;

    @FindBy(css = "input#email")
    private WebElement emailInput;

    @FindBy(css = "textarea#more")
    private WebElement tellUsAboutYourProjectInput;

    @FindBy(xpath = "//button[text() = 'Send']")
    private WebElement sendButton;

    @FindBy(xpath = "//div[text() = 'Name is required']")
    private WebElement nameIsRequiredWarning;

    @FindBy(xpath = "//div[text() = 'Email is required']")
    private WebElement emailIsRequiredWarning;

    @FindBy(xpath = "//div[text() = 'Description is required']")
    private WebElement descriptionIsRequiredWarning;

    @FindBy(xpath = "//div[text() = 'Must be a valid email']")
    private WebElement invalidEmailWarning;

    @FindBy(xpath = "//div[contains(text(), 'Received')]")
    private WebElement receivedConfirmation;

    @FindBy(xpath = "//a[text() = 'Schedule your free consultation now!']")
    private WebElement headerLink;

    @FindBy(xpath = "//strong[text() = 'candidates']/ancestor::section//a")
    private WebElement candidatesLink;

    @FindBy(xpath = "//strong[text() = 'increase']/ancestor::section//a")
    private WebElement increaseLink;

    @FindBy(xpath = "//strong[text() = 'free']/ancestor::section//a")
    private WebElement freeLink;

    @FindBy(xpath = "//strong[text() = 'level?']/ancestor::section//a")
    private WebElement nextLevelLink;

    @FindBy(xpath = "//h3//span[text() = 'Understanding your needs']")
    private WebElement understandingYourNeedsHeader;

    @FindBy(xpath = "//h3//span[text() = 'Sourcing candidates']")
    private WebElement sourcingCandidatesHeader;

    @FindBy(xpath = "//h3//span[text() = 'Technical evaluation']")
    private WebElement technicalEvaluationHeader;

    @FindBy(xpath = "//h3//span[text() = 'Cultural fit evaluation']")
    private WebElement culturalFitEvaluationHeader;

    @FindBy(xpath = "//h3//span[text() = 'Data-driven insights']")
    private WebElement dataDrivenInsightsHeader;

    @FindBy(xpath = "//h3//span[text() = 'Final selection and onboarding']")
    private WebElement finalSelectionHeader;

    @FindBy(xpath = "//span[contains(text(), 'work with you to understand')]")
    private WebElement understandingYourNeedsDetail;

    @FindBy(xpath = "//span[contains(text(), 'Our team of expert recruiters')]")
    private WebElement sourcingCandidatesDetail;

    @FindBy(xpath = "//span[contains(text(), 'technical skills and fit for your company')]")
    private WebElement technicalEvaluationDetail;

    @FindBy(xpath = "//span[contains(text(), 'technical skills and fit for your company')]")
    private WebElement culturalFitDetail;

    @FindBy(xpath = "//span[contains(text(), 'technical skills and fit for your company')]")
    private WebElement dataDrivenInsightsDetail;

    @FindBy(xpath = "//span[contains(text(), 'technical skills and fit for your company')]")
    private WebElement finalSelectionDetail;

    // Constructor
    public FrontPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.shortWait = new WebDriverWait(driver, 4);
        this.mediumWait = new WebDriverWait(driver, 8);
        this.longWait = new WebDriverWait(driver, 10);
    }

    // Open the front page
    public void open() {
        driver.get(PAGE_URL);
    }

    public boolean checkYourPartnerForHiringText() {
        return BasicMethods.checkElement(yourPartnerForHiringText, driver, mediumWait);
    }

    public boolean checkWeAssessCandidatesText() {
        return BasicMethods.checkElement(weAssessCandidatesText, driver, mediumWait);
    }

    public boolean checkhowWeHelpText() {
        return BasicMethods.checkElement(howWeCanHelpText, driver, mediumWait);
    }

    public boolean checkdiscoverHowWeHelpedText() {
        return BasicMethods.checkElement(discoverHowWeHelpedText, driver, mediumWait);
    }

    public boolean checkourProcessText() {
        return BasicMethods.checkElement(ourProcessText, driver, mediumWait);
    }

    public boolean checkOurTeamText() {
        return BasicMethods.checkElement(ourTeamText, driver, mediumWait);
    }

    public void enterName(String name) {
        BasicMethods.enterText(nameInput, driver, shortWait, name);
    }

    public void clearName() {
        BasicMethods.clearText(nameInput, driver, shortWait);
    }

    public void enterEmail(String email) {
        BasicMethods.enterText(emailInput, driver, shortWait, email);
    }

    public void clearEmail() {
        BasicMethods.clearText(emailInput, driver, shortWait);
    }

    public void enterDescription(String description) {
        BasicMethods.enterText(tellUsAboutYourProjectInput, driver, shortWait, description);
    }

    public void clearDescription() {
        BasicMethods.clearText(tellUsAboutYourProjectInput, driver, shortWait);
    }

    public void clickSendButton() {
        BasicMethods.click(sendButton, driver, shortWait);
    }

    public boolean checkNameIsRequiredWarning() {
        return BasicMethods.checkElement(nameIsRequiredWarning, driver, shortWait);
    }

    public boolean checkEmailIsRequiredWarning() {
        return BasicMethods.checkElement(emailIsRequiredWarning, driver, shortWait);
    }

    public boolean checkDescriptionIsRequiredWarning() {
        return BasicMethods.checkElement(descriptionIsRequiredWarning, driver, shortWait);
    }

    public boolean checkInvalidEmailWarning() {
        return BasicMethods.checkElement(invalidEmailWarning, driver, shortWait);
    }

    public boolean checkReceivedConfirmation() {
        return BasicMethods.checkElement(receivedConfirmation, driver, longWait);
    }

    public void clickHeaderLink() {
        BasicMethods.click(headerLink, driver, shortWait);
    }

    public void clickCandidatesLink() {
        BasicMethods.click(candidatesLink, driver, shortWait);
    }

    public void clickIncreaseLink() {
        BasicMethods.click(increaseLink, driver, shortWait);
    }

    public void clickFreeLink() {
        BasicMethods.click(freeLink, driver, shortWait);
    }

    public void clickNextLevelLink() {
        BasicMethods.click(nextLevelLink, driver, shortWait);
    }

    public void clickUnderstandingYourNeedsHeader() {
        BasicMethods.click(understandingYourNeedsHeader, driver, shortWait);
    }

    public void clickSourcingCandidatesHeader() {
        BasicMethods.click(sourcingCandidatesHeader, driver, shortWait);
    }

    public void clickTechnicalEvaluationHeader() {
        BasicMethods.click(technicalEvaluationHeader, driver, shortWait);
    }

    public void clickCulturalFitEvaluationHeader() {
        BasicMethods.click(culturalFitEvaluationHeader, driver, shortWait);
    }

    public void clickDataDrivenInsightsHeader() {
        BasicMethods.click(dataDrivenInsightsHeader, driver, shortWait);
    }

    public void clickfinalSelectionHeader() {
        BasicMethods.click(finalSelectionHeader, driver, shortWait);
    }

    public boolean checkUnderstandingYourNeedsDetail() {
        return BasicMethods.checkElement(understandingYourNeedsDetail, driver, mediumWait);
    }

    public boolean checkSourcingCandidatesDetail() {
        return BasicMethods.checkElement(sourcingCandidatesDetail, driver, mediumWait);
    }

    public boolean checkTechnicalEvaluationDetail() {
        return BasicMethods.checkElement(technicalEvaluationDetail, driver, mediumWait);
    }

    public boolean checkCulturalFitDetail() {
        return BasicMethods.checkElement(culturalFitDetail, driver, mediumWait);
    }

    public boolean checkDataDrivenInsightsDetail() {
        return BasicMethods.checkElement(dataDrivenInsightsDetail, driver, mediumWait);
    }

    public boolean checkFinalSelectionDetail() {
        return BasicMethods.checkElement(finalSelectionDetail, driver, mediumWait);
    }
}